# License  
  
**JustHelloWorld** &copy; by *Tim54000 \<Tim54000@gmx.com>*  
  
**JustHelloWorld** is licensed under a  
Creative Commons Attribution 4.0 International License \[CC-By].  
  
To view a copy of this license, visit [CreativeCommons.org](http://creativecommons.org/licenses/by/4.0/) or send a letter to *Creative Commons, PO Box 1866, Mountain View, CA 94042, USA*.