/// [AUTHORS]
///
/// Tim54000, main contributor
///
/// [LICENSE]
///
/// This work is licensed under the Creative Commons Attribution 4.0 International License [CC-BY].
/// To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

fn main() {
    println!("Hello, world!");
}
